HOME_PATH=~
CONFIG_PATH=~/.config
DOCUMENTS_PATH=~/Documents
USER_PATH=./user-settings

. ./modules/helpers.sh
. ./modules/post_setup_msg.sh

. ./modules/remap_caps_lock.sh
. ./modules/install_debian_pkgs.sh
. ./modules/generate_ssh_key.sh

init_option_1() {
  echo -e "\nYou chose Option 1..."
  remap_caps_lock
  install_debian_pkgs
  generate_ssh_key
  post_setup_msg_option_1
}

. ./modules/install_nodejs.sh
. ./modules/install_git.sh
. ./modules/install_flatpak_apps.sh
. ./modules/install_npm_pkgs.sh
. ./modules/install_ruby_rvm.sh
. ./modules/install_platform_tools.sh
. ./modules/install_crgrep.sh
. ./modules/launch_programs.sh

ADD_APT_REPO_PKGS="git"
TOOLS="adb-platform-tools crgrep nodejs ruby rvm"
init_option_2() {
  echo -e "\nYou chose Option 2..."
  install_nodejs
  install_git
  install_flatpak_apps
  install_npm_pkgs
  install_ruby_rvm
  install_platform_tools
  install_crgrep
  launch_programs
  post_setup_msg_option_2
}

. ./modules/copy_keyboard_config.sh
. ./modules/copy_bash_settings.sh
. ./modules/copy_git_settings.sh
. ./modules/copy_profile_settings.sh
. ./modules/copy_suspend_settings_v1.1.sh
. ./modules/copy_backup_scripts.sh
. ./modules/copy_autokey_settings.sh
. ./modules/copy_xbindkeys_settings.sh
. ./modules/copy_copyq_settings.sh
. ./modules/copy_libreoffice_settings.sh
. ./modules/copy_codium_settings.sh
. ./modules/copy_xfce_settings.sh
. ./modules/make_directories.sh

init_option_3() {
  echo -e "\nYou chose Option 3..."
  copy_keyboard_config
  copy_bash_settings
  copy_git_settings
  copy_profile_settings
  copy_suspend_settings
  copy_backup_scripts
  copy_autokey_settings
  copy_xbindkeys_settings
  copy_copyq_settings
  copy_libreoffice_settings
  copy_codium_settings
  copy_xfce_settings
  make_directories
  post_setup_msg_option_3
}

. ./modules/init_setup_options.sh

echo -e "\nWelcome to your Linux Mint Xfce setup script!"
while true; do
  echo ""
  read -p "Do you want to continue? [Y,n] " answer
  case $answer in
    [Yy]* ) init_setup_options;;
    [Nn]* ) exit_cmd;;
    * ) echo "Please answer yes [Yy] or no [Nn].\n";;
  esac
done
