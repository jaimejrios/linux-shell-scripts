# Linux Grunt Scripts

These Linux grunt scripts assist with doing your grunt work after installing Linux.

They copy over essential files and settings, as well as help install important programs (via the apt package manager).

The scripts are extremely helpful when switching Linux distributions (aka distro hopping) or installing/re-installing Linux. This repo includes the runner and module scripts.

I wrote these scripts for use on Linux Mint (Xfce edition), but they can be easily altered to suit your use cases.

## How To Execute The Grunt Scripts

1) Clone the `linux-grunt-scripts/` repo into `~/Downloads/`
2) Open up the Terminal, then `cd` into `~/Downloads/linux-grunt-scripts/`
3) Inside the Terminal, execute the runner/setup script by typing `./setup.sh` and hitting `Enter`

## How The Runner/Setup Script Works

After the runner script is executed, the user will be prompted to `continue` or `exit` the setup.

If the user chooses to `continue`, the runner script will prompt the user to select a particular phase for installing specific files/applications.

Choosing `exit` will quit the script.

## Setting Up Ungoogled-Chromium On Linux

After running the grunt scripts, here's how to setup Ungoogled-Chromium on Linux:

1) Enter in `chrome://flags` in the Chromium address bar and search for the `#extension-mime-request-handling` flag and set it to `Always prompt for install`.
2) Next, download the .crx from [Releases](https://github.com/NeverDecaf/chromium-web-store/releases/), where you'll be prompted to install the extension.

> Hooray! Now you can install Chrome extenstions like [OneTab](https://www.one-tab.com/), [BitWarden](https://bitwarden.com/), and [Ublock Origin](https://ublockorigin.com/).

Annnd... let's add the Google search to Ungoogled-Chromium (since the browser doesn't ship with it by default):

1) Go to `chrome://settings` in the address bar and click on `Manage search engines and site search`
2) Scroll down to `Site Search` and click on `Add`
3) Enter in the following data into the form field:
    - **Name**: `Google`
    - **Shortcut**: `google.com`
    - **URL with %s in place of query**: `https://www.google.com/search?q=%s`
    - **Suggested URL with %s in place of query**: `https://www.google.com/complete/search?client=chrome&q=%s`
