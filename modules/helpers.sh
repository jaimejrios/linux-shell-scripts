print_block() {
  echo -e "\n==============="
}

check_if_path_files_exist() {
  path=$1
  path_copy=$2
  for file in `ls $path_copy`; do
    if [ ! -f "$path/$file" ]; then
      echo -e "> '$file' was not found!"
      return "1"
    fi
  done
  return "0"
}

invoke_func_if_instance() {
  if [ "$INSTANCE" = "1" ]; then
    $1
    wait
    sleep 2s
    echo -e "\nModule script has completed. Goodbye...\n"
  fi
}

make_directory() {
  path=$1
  sleep 2s
  echo -e "\nChecking if '$path' directory exists..."

  if [ -d $path ] ; then
    sleep 2s
    echo -e "> '$path' was found! Moving on..."

  else
    sleep 2s
    echo -e "> '$path' was not found..."
    sleep 1s
    echo -e "\nCreating a new $path directory..."
    mkdir $path
    sleep 2s
    echo -e "Done!"
  fi
}

exit_cmd() {
  sleep 1s
  echo -e "\nExiting the setup script..."
  sleep 1s
  exit 0
}
