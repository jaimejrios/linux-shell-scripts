if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

bashrc_file=~/.bashrc
bashrc_copy=$USER_PATH/.bashrc
git_completion_copy=$USER_PATH/.git-completion.bash
git_prompt_copy=$USER_PATH/.git-prompt.sh

copy_bash_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying Bash settings..."
  sleep 2s
  echo -e "\nChecking if '$bashrc_file' file exists..."
  sleep 2s

  if [ ! -f $bashrc_file ] ; then
    echo -e "'$bashrc_file' file was not found..."
    sleep 2s
    echo -e "\nPlease create a new .bashrc file in your home directory.\n> Use the command 'touch ~/.bashrc'"

  else
    echo -e "> '$bashrc_file' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if user bash settings exist in '$bashrc_file' file..."
    sleep 2s

    if grep -F -q -x -v -f $bashrc_file $bashrc_copy; then

      echo -e "> Bash user settings don't exist!"
      sleep 2s
      echo -e "\nBacking up current '$bashrc_file' settings into '$DOCUMENTS_PATH'"
      sudo cp $bashrc_file $DOCUMENTS_PATH
      sleep 2s
      echo -e "Done!"
      sleep 2s
      echo -e "\nCopying '$bashrc_copy' settings into $HOME_PATH"
      cat $bashrc_copy >> $bashrc_file
      cp $git_completion_copy ~/
      cp $git_prompt_copy ~/
      sleep 2s
      echo -e "Done!"

    else
      echo -e "> Bash user settings already exist! Moving on..."
    fi

  fi
}

invoke_func_if_instance copy_bash_settings
