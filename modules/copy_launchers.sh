if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

launchers_path=~/.local/share/applications
launchers_mirror=$USER_PATH/.local/share/applications
launcher_files=$USER_PATH/.local/share/applications/*

copy_launchers() {
  print_block
  sleep 2s
  echo -e "\nNow copying desktop launcher files..."
  sleep 2s
  echo -e "\nChecking if '$launchers_path' exists..."
  sleep 2s

  if [ ! -d $launchers_path ] ; then

    echo -e "> '$launchers_path' was not found..."
    sleep 2s
    echo -e "> '*.desktop' launcher files were not copied!"

  else

    echo -e "> $launchers_path was found! Examining..."
    sleep 2s

    echo -e "\nChecking if all launcher files exist..."
    check_if_path_files_exist $launchers_path $launchers_mirror

    files_exist=$?
    sleep 2s

    if [ "$files_exist" = "0" ] ; then

      echo -e "> All launcher files were found! Moving on..."

    else

      echo -e "> Launcher files do not exist..."
      sleep 2s

      echo -e "\nCopying launcher files into $launchers_path"
      cp $launcher_files $launchers_path
      sleep 2s
      echo -e "Done!"

    fi
  fi
}

invoke_func_if_instance copy_launchers
