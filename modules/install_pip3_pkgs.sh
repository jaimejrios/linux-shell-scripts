if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

pip3_path=/usr/bin/pip3
pip3_pkgs="flask markdown2 pillow pylint youtube-dl"

install_pip3_pkgs() {
  print_block
  sleep 2s
  echo -e "\nNow installing your pip3 packages..."
  sleep 2s
  echo -e "\nChecking if pip3 is installed..."
  sleep 2s
  if [ -e $pip3_path ]; then
    echo -e "> pip3 is installed!"
    echo -e "> Installing pip3 packages now...\n"
    pip3 install --upgrade
    pip3 install $pip3_pkgs
#    pip3 install flask
#    pip3 install markdown2
#    pip3 install pillow
#    pip3 install pylint
#    pip3 install youtube_dl
    wait
  else
    echo -e "> pip3 is not installed..."
    sleep 2s
    echo -e "> pip3 packages were not installed..."
  fi
}

invoke_func_if_instance install_pip3_pkgs
