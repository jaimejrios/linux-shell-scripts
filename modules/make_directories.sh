if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

#applications_path=~/Applications
projects_path=~/Projects
directories_list="/Projects"

make_directories() {
  print_block
  sleep 2s
  echo -e "\nNow creating directories..."
#  make_directory $applications_path
  make_directory $projects_path
}

invoke_func_if_instance make_directories
