if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

debian_pkgs="autokey-qt catfish copyq flatpak ffmpeg gpick ibus imagemagick libvips picard software-properties-common webp xbindkeys xclip xdotool"

install_debian_pkgs() {
  print_block
  sleep 2s
  echo -e "\nNow installing your debian packages..."
  sudo apt install $debian_pkgs -y
  wait
}

invoke_func_if_instance install_debian_pkgs
