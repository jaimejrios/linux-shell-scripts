if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

enable_suspend_file=/etc/polkit-1/rules.d/enable-suspend.rules
enable_suspend_copy=$USER_PATH/etc/polkit-1/rules.d/enable-suspend.rules

polkit_rules_path=/etc/polkit-1/rules.d

copy_suspend_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying suspend settings..."
  sleep 2s
  echo -e "\nChecking if 'enable-suspend.rules' file exists..."
  sleep 2s

  if sudo test -f $enable_suspend_file ; then

    echo -e "> 'enable-suspend.rules' was found!"
    sleep 2s
    echo -e "> Moving on!"

  else

    echo -e "> 'enable-suspend.rules' does not exist!"
    sleep 2s
    echo -e "\nCopying file now..."
    sudo cp $enable_suspend_copy $polkit_rules_path
    sleep 2s
    echo -e "Done!"

  fi

}

invoke_func_if_instance copy_suspend_settings
