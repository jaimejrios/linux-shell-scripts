if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

xfce_config_path=~/.config/xfce4
xfce_default_settings=~/.config/xfce4/xfconf/xfce-perchannel-xml
xfce_default_settings_file=~/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml

xfce_user_settings=$USER_PATH/.config/xfce4/xfconf/xfce-perchannel-xml/*
xfce_user_settings_file=$USER_PATH/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml
# xfce_user_terminal_settings=$USER_PATH/.config/xfce4/terminal

copy_xfce_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying Xfce settings..."
  sleep 2s
  echo -e "\nChecking if '$xfce_config_path' directory exists..."
  sleep 2s

  if [ -d $xfce_config_path ] ; then

    echo -e "> '$xfce_config_path' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if Xfce user settings exist..."
    sleep 2s

    if cmp --silent -- $xfce_default_settings_file $xfce_user_settings_file; then
      echo -e "> Xfce user settings are already installed!"
      sleep 1s
      echo -e "> Moving on..."
    else
      echo -e "\nXfce user settings were not found..."
      sleep 2s
      echo -e "Now copying Xfce terminal configuration and settings..."
      cp $xfce_user_settings $xfce_default_settings
      # cp -r $xfce_user_terminal_settings $xfce_config_path
      sleep 2s
      echo -e "Done!"

    fi

  else

    echo -e "> '$xfce_settings_path' directory was not found..."
    sleep 2s
    echo -e "Xfce user settings were not copied...\n"

  fi
}

invoke_func_if_instance copy_xfce_settings
