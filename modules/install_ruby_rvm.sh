if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

ruby_path=/usr/bin/ruby
rvm_ruby_path=~/.rvm/rubies/ruby-2.7.4/bin/ruby

install_rvm() {
  sudo apt update
  sudo apt install gnupg2
  gpg2 --keyserver keyserver.ubuntu.com --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
  \curl -sSL https://get.rvm.io | bash -s stable
  source ~/.rvm/scripts/rvm
}

install_ruby() {
  rvm pkg install openssl
  rvm install 2.7.4 --with-openssl-dir=$HOME/.rvm/usr
}

install_ruby_rvm() {
  print_block
  sleep 2s
  echo -e "\nNow installing RVM and Ruby..."
  sleep 2s
  echo -e "\nChecking if Ruby is installed..."
  sleep 2s
  if [ -e $ruby_path ]; then
    echo -e "> Ruby is installed!"
    sleep 2s
    echo -e "> Removing Ruby installation to make way for RVM...\n"
    sleep 2s
    sudo apt purge ruby -y
    echo -e "> Now installing RVM..."
    sleep 2s
    install_rvm
    echo -e "> RVM was installed!"
    sleep 2s
    echo -e "> Now installing Ruby via RVM..."
    sleep 2s
    install_ruby
  else
    echo -e "> Ruby is not installed...\n"
    sleep 2s
    echo -e "Now checking for RVM (and Ruby)..."
    sleep 2s
    if [ -e $rvm_ruby_path ]; then
      echo -e "> RVM and Ruby are already installed!"
      sleep 2s
      echo -e "> Moving on..."
    else
      echo -e "> RVM and Ruby aren't installed..."
      sleep 2s
      echo -e "> Installing Ruby and RVM now..."
      sleep 2s
      install_rvm
      sleep 2s
      install_ruby
    fi
  fi
}

invoke_func_if_instance install_ruby_rvm
