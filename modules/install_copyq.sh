if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

install_copyq() {
  print_block
  sleep 2s
  echo -e "\nNow installing CopyQ..."
  sleep 2s
  sudo add-apt-repository ppa:hluk/copyq
  sudo apt update
  sudo apt install copyq -y
  wait
}

invoke_func_if_instance install_copyq
