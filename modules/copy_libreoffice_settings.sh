if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

libreoffice_settings_path=~/.config/libreoffice/4
libreoffice_default_settings=~/.config/libreoffice/4/user/registrymodifications.xcu

libreoffice_settings_copy=$USER_PATH/.config/libreoffice/4/user
libreoffice_user_settings=$USER_PATH/.config/libreoffice/4/user/registrymodifications.xcu

copy_libreoffice_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying LibreOffice settings..."
  sleep 2s
  echo -e "\nChecking if '$libreoffice_settings_path' directory exists..."
  sleep 2s

  if [ -d $libreoffice_settings_path ] ; then

    echo -e "> '$libreoffice_settings_path' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if LibreOffice user settings exist..."
    sleep 2s

    if cmp --silent -- $libreoffice_default_settings $libreoffice_user_settings; then
      echo -e "> LibreOffice user settings are already installed!"
      sleep 1s
      echo -e "> Moving on..."
    else
      echo -e "\nLibreOffice user settings were not found..."
      sleep 2s
      echo -e "Copying LibreOffice user settings into $libreoffice_settings_path"
      cp -r $libreoffice_settings_copy $libreoffice_settings_path
      sleep 2s
      echo -e "Done!"
    fi

  else
    echo -e "> '$libreoffice_settings_path' directory was not found..."
    sleep 2s
    echo -e "LibreOffice user settings were not copied..."
  fi
}

invoke_func_if_instance copy_libreoffice_settings
