if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

adb_path=~/.adb
platform_tools_zip=$USER_PATH/platform-tools/platform-tools_*.zip

install_platform_tools() {
  print_block
  sleep 2s
  echo -e "\nNow installing ADB Platform Tools..."
  sleep 1s
  echo -e "\nChecking if '$adb_path' directory exists..."
  sleep 2s

  if [ -d $adb_path ] ; then
    echo -e "> '$adb_path' was found! Moving on..."
  else
    echo -e "> '$adb_path' was not found..."
    sleep 2s
    echo -e "\nInstalling ADB Platform Tools..."
    sudo mkdir ~/.adb
    sudo unzip $platform_tools_zip -d $adb_path
    echo -e "Done!"
  fi
}

invoke_func_if_instance install_platform_tools
