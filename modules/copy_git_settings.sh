if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

gitconfig_file=~/.gitconfig
gitignore_file=~/.gitignore_global

gitconfig_copy=$USER_PATH/.gitconfig
gitignore_copy=$USER_PATH/.gitignore_global

copy_git_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying Git config settings..."
  sleep 2s
  echo -e "\nChecking if '$gitconfig_file' and '$gitignore_file' files exist..."
  sleep 2s

  if [ -f $gitconfig_file -a -f $gitignore_file ] ; then
    echo -e "> '$gitconfig_file' and '$gitignore_file' files were found!"
    sleep 1s
    echo -e "> Moving on..."
  else

    echo -e "'$gitconfig_file' and '$gitignore_file' files were not found."
    sleep 2s
    echo -e "\nCopying gitconfig settings into $HOME_PATH"
    cp $gitconfig_copy $HOME_PATH
    cp $gitignore_copy $HOME_PATH
    sleep 2s
    echo -e "Done!"

  fi
}

invoke_func_if_instance copy_git_settings
