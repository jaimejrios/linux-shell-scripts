if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

enable_suspend_file=/etc/polkit-1/localauthority/50-local.d/enable-suspend.pkla
enable_suspend_copy=$USER_PATH/etc/polkit-1/localauthority/50-local.d/enable-suspend.pkla

polkit_local_path=/etc/polkit-1/localauthority/50-local.d

copy_suspend_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying suspend settings..."
  sleep 2s
  echo -e "\nChecking if 'enable-suspend.pkla' file exists..."
  sleep 2s

  if sudo test -f $enable_suspend_file ; then

    echo -e "> 'enable-suspend.pkla' was found!"
    sleep 2s
    echo -e "> Moving on!"

  else

    echo -e "> 'enable-suspend.pkla' does not exist!"
    sleep 2s
    echo -e "\nCopying file now..."
    sudo cp $enable_suspend_copy $polkit_local_path
    sleep 2s
    echo -e "Done!"

  fi

}

invoke_func_if_instance copy_suspend_settings
