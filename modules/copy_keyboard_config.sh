if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

default_path=/etc/default
keyboard_config_path=/etc/default/keyboard
keyboard_config_copy=$USER_PATH/etc/default/keyboard

copy_keyboard_config() {
  print_block
  sleep 2s
  echo -e "\nNow copying keyboard config file..."
  sleep 2s
  echo -e "\nChecking if user settings exist..."
  sleep 2s

  if cmp --silent -- $keyboard_config_path $keyboard_config_copy; then
    echo -e "> User's config settings are already installed!"
    sleep 1s
    echo -e "> Moving on..."
  else
    echo -e "\nUser's config settings were not found..."
    sleep 2s
    echo -e "Copying keyboard config file into '$default_path'"
    sudo cp $keyboard_config_copy $default_path
    sleep 2s
    echo -e "Done!"
  fi

}

invoke_func_if_instance copy_keyboard_config
