if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

copyq_settings_path=~/.config/copyq
copyq_default_settings=~/.config/copyq/copyq.conf

copyq_settings_copy=$USER_PATH/.config/copyq/*
copyq_user_settings=$USER_PATH/.config/copyq/copyq.conf

copy_copyq_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying CopyQ settings..."
  sleep 2s
  echo -e "\nChecking if '$copyq_settings_path' directory exists..."
  sleep 2s

  if [ -d $copyq_settings_path ] ; then

    echo -e "> '$copyq_settings_path' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if CopyQ user settings exist..."
    sleep 2s

    if cmp --silent -- $copyq_default_settings $copyq_user_settings; then
      echo -e "> CopyQ user settings are already installed!"
      sleep 1s
      echo -e "> Moving on..."
    else
      echo -e "\nCopyQ user settings were not found..."
      sleep 2s
      echo -e "Copying CopyQ user settings into '$copyq_settings_path'"
      cp -r $copyq_settings_copy $copyq_settings_path
      sleep 2s
      echo -e "Done!"
    fi

  else
    echo -e "> '$copyq_settings_path' directory was not found..."
    sleep 2s
    echo -e "CopyQ user settings were not copied..."
  fi
}

invoke_func_if_instance copy_copyq_settings
