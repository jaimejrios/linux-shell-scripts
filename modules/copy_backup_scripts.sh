if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

backup_script=~/backup_cloud_usb.sh
cloud_backup_script=~/cloud_to_usb_backup.sh
wget_bookmarks_script=~/wget_bookmarks.sh

backup_script_copy=$USER_PATH/backup_cloud_usb.sh
cloud_backup_script_copy=$USER_PATH/cloud_to_usb_backup.sh
wget_bookmarks_script_copy=$USER_PATH/wget_bookmarks.sh

copy_backup_scripts() {
  print_block
  sleep 2s
  echo -e "\nNow copying backup scripts..."
  sleep 2s
  echo -e "\nChecking if backup script files exist in the home directory..."
  sleep 2s

  if [ ! -f $backup_script -o ! -f $cloud_backup_script -o ! -f $wget_bookmarks_script ] ; then
    echo -e "One (or more) backup script files were not found in the home directory."
    sleep 2s
    echo -e "\nCopying backup scripts into $HOME_PATH"
    cp $backup_script_copy ~/
    cp $cloud_backup_script_copy ~/
    cp $wget_bookmarks_script_copy ~/
    sleep 2s
    echo -e "Done!"
  else
    echo -e "> Backup script files already exist! Moving on..."
  fi

}

invoke_func_if_instance copy_backup_scripts
