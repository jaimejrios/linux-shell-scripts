if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

xbindkeysrc_path=~/.xbindkeysrc
xbindkeysrc_copy=$USER_PATH/.xbindkeysrc

copy_xbindkeys_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying xbindkeys settings..."
  sleep 2s
  echo -e "\nChecking if '$xbindkeysrc_path' exists..."
  sleep 2s

  if [ -f $xbindkeysrc_path ] ; then
    echo -e "> '$xbindkeysrc_path' was found!"
    sleep 1s
    echo -e "> Moving on..."
  else

    echo -e "'$xbindkeysrc_path' not found."
    sleep 2s
    echo -e "\nCopying xbindkeys settings into $HOME_PATH"
    cp $xbindkeysrc_copy $HOME_PATH
    sleep 2s
    echo -e "Done!"

  fi
}

invoke_func_if_instance copy_xbindkeys_settings
