if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

autokey_path=~/.config/autokey
autokey_data_path=~/.config/autokey/data/autokey-hyper-scripts
autokey_data_copy=$USER_PATH/.config/autokey/data

copy_autokey_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying Autokey settings..."
  sleep 2s
  echo -e "\nChecking if '$autokey_path' directory exists..."
  sleep 2s

  if [ -d $autokey_path ] ; then
    echo -e "> '$autokey_path' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if '$autokey_data_path' directory exists..."
    sleep 2s

    if [ -d $autokey_data_path ] ; then
      echo -e "> '$autokey_data_path' was found!"
      sleep 1s
      echo -e "> Moving on..."

    else
      echo -e "> '$autokey_data_path' was not found..."
      sleep 2s
      echo -e "\nCopying autokey settings into $autokey_path"
      cp -r $autokey_data_copy $autokey_path
      sleep 2s
      echo -e "Done!"

    fi
  else
    echo -e "> '$autokey_path' directory was not found..."
    sleep 2s
    echo -e "Please install Autokey, then open the program to create the directory."
    sleep 1s
    echo -e "Afterwards, re-run the setup script to copy Autokey settings."
  fi
}

invoke_func_if_instance copy_autokey_settings
