if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

install_git() {
  print_block
  sleep 2s
  echo -e "\nNow installing Git..."
  sudo add-apt-repository -y ppa:git-core/ppa
  sudo apt update
  sudo apt install git -y
  wait
}

invoke_func_if_instance install_git
