if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

profile_path=~/.profile
profile_copy=$USER_PATH/.profile

copy_profile_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying '$profile_path' settings..."
  sleep 2s
  echo -e "\nChecking if '$profile_path' exists..."
  sleep 2s

  if [ ! -f $profile_path ] ; then
    echo -e "'$profile_path' was not found..."
    sleep 2s
    echo -e "\nUse the command 'touch ~/.profile' to create this file inside your home directory."

  else
    echo -e "> '$profile_path' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if your settings exist inside '$profile_path'"
    sleep 2s

    if grep -F -q -x -v -f $profile_path $profile_copy; then

      echo -e "> Your '$profile_path' settings don't exist!"
      sleep 2s
      echo -e "\nBacking up '$profile_path' to '$DOCUMENTS_PATH'"
      sudo cp $profile_path $DOCUMENTS_PATH
      sleep 2s
      echo -e "Done!"
      sleep 2s
      echo -e "\nNow copying your '$profile_path' settings."
      cat $profile_copy >> $profile_path
      sleep 2s
      echo -e "Done!"

    else
      echo -e "> Your '$profile_path' settings already exist! Moving on..."
    fi

  fi
}

invoke_func_if_instance copy_profile_settings
