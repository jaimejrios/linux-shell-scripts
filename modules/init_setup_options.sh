ask_to_continue() {
  while true; do
    print_block
    echo -e ""
    read -p "Do you want to setup something else? [Y,n] " answer
    case $answer in
      [Yy]* ) init_setup_options;;
      [Nn]* ) exit_cmd;;
      * ) echo -e "Please answer yes [Yy] or no [Nn].\n";;
    esac
  done
}

init_setup_options() {
  echo -e "\nOkay! Here are your setup options..."
  sleep 1.5s

  echo -e "\n1) Option 1:"
  echo -e "  - Remap Caps Lock to the Hyper_L key."
  echo -e "  - Install user packages via 'sudo apt install ...'"
  echo -e "  - Generate a ssh key for online version control."

  echo -e "\n2) Option 2:"
  echo -e "  - Install '$flatpak_apps' with Flatpak"
  echo -e "  - Install '$ADD_APT_REPO_PKGS' with add-apt-repository."
  echo -e "  - Install '$TOOLS' tools."

  echo -e "\n3) Option 3:"
  echo -e "  - Copy $USER_PATH files to $HOME_PATH"
  echo -e "  - Install Xfce desktop settings."
  echo -e "  - Create $directories_list folder(s) inside $HOME_PATH"

  echo -e "\n0) Exit setup script."

  while true; do
    echo -e ""
    read -p "Please enter in your option: " option
    case $option in
      [1]* ) init_option_1; ask_to_continue;;
      [2]* ) init_option_2; ask_to_continue;;
      [3]* ) init_option_3; ask_to_continue;;
      [0]* ) exit_cmd;;
      * ) echo -e "\nPlease enter an option [1,2,3] or type '0' to exit";;
    esac
  done
}
