if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

var_app_path=~/.var/app

codium_path=~/.var/app/com.vscodium.codium
codium_user_path=~/.var/app/com.vscodium.codium/config/VSCodium/User/
codium_default_settings=~/.var/app/com.vscodium.codium/config/VSCodium/User/settings.json

codium_user_prefs=$USER_PATH/.var/app/com.vscodium.codium/config/VSCodium/User/*
codium_user_settings=$USER_PATH/.var/app/com.vscodium.codium/config/VSCodium/User/settings.json


copy_codium_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying VSCodium settings..."
  sleep 2s
  echo -e "\nChecking if '$codium_path' directory exists..."
  sleep 2s

  if [ -d $codium_path ] ; then
    echo -e "> '$codium_path' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if VSCodium settings exist..."
    sleep 2s

    if cmp --silent -- $codium_default_settings $codium_user_settings; then
      echo -e "> VSCodium settings are already installed!"

    else
      echo -e "\nVSCodium settings were not found."
      sleep 2s
      echo -e "Copying files now..."
      cp $codium_user_prefs $codium_user_path
      sleep 2s
      echo -e "Done!"
    fi

  else
    echo -e "> '$codium_path' was not found."
    sleep 2s
    echo -e "> VSCodium settings were not installed."
    sleep 2s
    echo -e "Done!"
  fi
}

invoke_func_if_instance copy_codium_settings
