
post_setup_msg_option_1() {
  sleep 2s
  echo -e "\nThe setup script for Option 1 is complete!"
  sleep 2s
  echo -e "Make sure to add your copied SSH key to your online Git account."
}

post_setup_msg_option_2() {
  sleep 2s
  echo -e "\nThe setup script for Option 2 is complete!"
}

post_setup_msg_option_3() {
  sleep 2s
  echo -e "\nThe setup script for Option 3 is complete!"
}

post_setup_msg_option_4() {
  sleep 2s
  echo -e "\nThe setup script for Option 4 is complete!"
}
