if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

programs="autokey-qt libreoffice copyq vscodium"

launch_programs() {
  print_block
  sleep 2s
  echo -e "\nLaunching '$programs' to initialize program settings..."
  autokey-qt&
  sleep 2s
  ps aux | grep autokey-qt | cut -d " " -f 9 | head -n 1 | xargs kill
  sleep 1s
  libreoffice&
  sleep 2s
  ps aux | grep libreoffice | cut -d " " -f 9 | head -n 1 | xargs kill
  sleep 1s
  copyq&
  sleep 2s
  ps aux | grep copyq | cut -d " " -f 9 | head -n 1 | xargs kill
  sleep 1s
  flatpak run com.vscodium.codium
  sleep 1s
}

invoke_func_if_instance launch_programs
