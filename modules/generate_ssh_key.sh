if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

email="riosj@protonmail.com"

generate_ssh_key() {
  print_block
  sleep 2s
  echo -e "\nNow generating your SSH key..."
  sleep 2s
  ssh-keygen -t ed25519 -C "<$email>"
  xclip -sel clip < ~/.ssh/id_ed25519.pub
  wait
}

invoke_func_if_instance generate_ssh_key
