if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

nodejs_path=/usr/bin/nodejs

install_nodejs() {
  print_block
  sleep 2s
  echo -e "\nNow installing nodejs..."
  sleep 2s
  echo -e "\nFirst checking if nodejs is installed..."
  sleep 2s
  if [ -e $nodejs_path ]; then
    echo -e "> nodejs is already installed! Moving on..."

  else
    echo -e "> nodejs is not installed..."
    sleep 2s
    echo -e "> Installing nodejs now..."
    sudo apt-get update
    sudo apt-get install -y ca-certificates curl gnupg
    sudo mkdir -p /etc/apt/keyrings
    curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg

    NODE_MAJOR=20
    echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list

    sudo apt-get update
    sudo apt-get install nodejs -y
    wait
  fi
}

invoke_func_if_instance install_nodejs
