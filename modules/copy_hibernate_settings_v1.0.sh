if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

polkit_local_path=/etc/polkit-1/localauthority/50-local.d

enable_hibernate_file=/etc/polkit-1/localauthority/50-local.d/enable-hibernate.pkla
enable_hibernate_copy=$USER_PATH/etc/polkit-1/localauthority/50-local.d/enable-hibernate.pkla

suspend_then_hibernate_service=/usr/lib/systemd/system/systemd-suspend-then-hibernate.service
suspend_service_symlink=/etc/systemd/system/systemd-suspend.service

copy_hibernate_settings() {
  print_block
  sleep 2s
  echo -e "\nNow copying hibernate settings..."
  sleep 2s
  echo -e "\nChecking if 'enable-hibernate.pkla' file exists..."
  sleep 2s

  if sudo test -f $enable_hibernate_file ; then

    echo -e "> 'enable-hibernate.pkla' was found!"
    sleep 2s
    echo -e "> Moving on!"

  else

    echo -e "> 'enable-hibernate.pkla' does not exist!"
    sleep 2s
    echo -e "\nCopying file now..."
    sudo cp $enable_hibernate_copy $polkit_local_path
    sleep 2s
    echo -e "Done!"

    sleep 2s
    echo -e "\nCreating a symlink for the 'suspend-on-hibernate' service..."
    sudo ln -s $suspend_then_hibernate_service $suspend_service_symlink
    sleep 2s
    echo -e "Done!"

  fi

}

invoke_func_if_instance copy_hibernate_settings
