if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

flatpak_path=/usr/bin/flatpak
flatpak_apps="Calibre KSnip NextCloud ProtonVPN Shotwell Standard-Notes Ungoogled-Chromium VSCodium"

install_flatpak_apps() {
  print_block
  sleep 2s
  echo -e "\nNow installing your Flatpak apps..."
  sleep 2s
  echo -e "\nFirst checking if Flatpak is installed..."
  sleep 2s
  if [ -e $flatpak_path ]; then
    echo -e "> Flatpak is installed!"
    echo -e "> Installing flatpak apps now...\n"
    flatpak install flathub com.calibre_ebook.calibre -y
    flatpak install flathub org.ksnip.ksnip -y
    flatpak install flathub com.nextcloud.desktopclient.nextcloud -y
    flatpak install flathub com.protonvpn.www -y
    flatpak install flathub org.gnome.Shotwell -y
    flatpak install flathub org.standardnotes.standardnotes -y
    flatpak install flathub com.github.Eloston.UngoogledChromium -y
    flatpak install flathub com.vscodium.codium -y
    wait
  else
    echo -e "> Flatpak is not installed..."
    sleep 2s
    echo -e "> Flatpak apps were not installed..."
  fi
}

invoke_func_if_instance install_flatpak_apps
