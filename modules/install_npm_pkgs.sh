if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

npm_path=/usr/bin/npm

install_npm_pkgs() {
  print_block
  sleep 2s
  echo -e "\nNow installing your npm packages..."
  sleep 2s
  echo -e "\nChecking if npm is installed..."
  sleep 2s
  if [ -e $npm_path ]; then
    echo -e "> npm is installed!"
    echo -e "> Installing npm packages now...\n"
    sudo npm install -g gulp-cli
    sudo npm install -g gulp
    sudo npm install -g node-gyp
    wait
  else
    echo -e "> npm is not installed..."
    sleep 2s
    echo -e "> npm packages were not installed..."
  fi
}

invoke_func_if_instance install_npm_pgks
