if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

xkb_path=/usr/share/X11/xkb/symbols
xkb_default_settings_file=/usr/share/X11/xkb/symbols/pc

#xkb_user_path=$USER_PATH/xkb
xkb_user_settings_file=$USER_PATH/xkb/pc

remap_caps_lock() {
  print_block
  sleep 2s
  echo -e "\nNow remapping Caps Lock with the Hyper_L key."
  sleep 2s
  echo -e "\nChecking if '$xkb_path' directory exists..."
  sleep 2s

  if [ -d $xkb_path ]; then
    echo -e "> '$xkb_path' was found! Examining..."
    sleep 2s
    echo -e "\nNow checking if Caps Lock has been remapped..."
    sleep 2s

    if grep -q "Hyper_L" "$xkb_default_settings_file" && ! grep -q "{ Caps_Lock }" "$xkb_default_settings_file"; then
      echo -e "> Caps Lock has already been remapped to Hyper_L"

    else
      echo -e "> Caps Lock has not been remapped to Hyper_L! Modifying key now..."
      sleep 2s
      echo -e "\nBacking up default keymap settings to '$DOCUMENTS_PATH'"
      sudo cp $xkb_default_settings_file $DOCUMENTS_PATH/$(hostname)-default-keymap-$(date '+%Y-%m-%d')
      sleep 2s
      echo -e "Done!"
      sleep 2s
      echo -e "\nRemapping the Caps Lock key to Hyper_L inside '$xkb_path'"
      sudo sed -i '/{ Caps_Lock }/d' $xkb_default_settings_file
      sudo sed -i "s/Caps_Lock/Hyper_L/g" $xkb_default_settings_file
      sleep 2s
      echo -e "Done!"
    fi

  else
    echo -e "> '$xkb_path' directory was not found..."
    sleep 2s
    echo -e "\nThe Caps Lock key was not remapped."
  fi

}

invoke_func_if_instance remap_caps_lock
