if [ "$USER_PATH" = "" ]; then
  USER_PATH=../user-settings
  INSTANCE="1"
  . ./helpers.sh
fi

crgrep_path=/opt/crgrep-1.0.5
crgrep_zip=$USER_PATH/crgrep/crgrep-*.zip

install_crgrep(){
  print_block
  sleep 2s
  echo -e "\nNow installing crgrep..."
  sleep 2s
  echo -e "\nChecking if crgrep is installed..."
  sleep 2s

  if [ -d $crgrep_path ] ; then
    echo -e "> crgrep is already installed! Moving on..."

  else

    echo -e "> crgrep is not installed..."
    sleep 2s
    echo -e "\nInstalling crgrep now..."
    sudo unzip $crgrep_zip -d /opt
    sudo sed -i '/^exec "$JAVACMD" $JAVA_OPTS.*/i JAVA_OPTS="--illegal-access=deny"\n' /opt/crgrep-*/bin/crgrep
    sleep 2s
    echo -e "> crgrep install complete!"
    sleep 2s
    echo -e "\nNow creating a symbolic link for crgrep in /usr/bin..."
    sudo ln -s /opt/crgrep-*/bin/crgrep /usr/bin/crgrep
    sleep 2s
    echo -e "> Done! Moving on..."

  fi

}

invoke_func_if_instance install_crgrep
