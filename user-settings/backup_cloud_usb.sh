
USB1_NAME=VERB128
USB2_NAME=VERB128D
#USB_TRASH=".Trash-1000"

USB1_PATH=/media/$(whoami)/$USB1_NAME
#USB1_TRASH_PATH=/media/$(whoami)/$USB1_NAME/$USB_TRASH

USB2_PATH=/media/$(whoami)/$USB2_NAME
#USB2_TRASH_PATH=/media/$(whoami)/$USB2_NAME/$USB_TRASH

CLOUD_NAME=Nextcloud
CLOUD_PATH=/home/$(whoami)/Nextcloud

if [ -z "$1" ]; then
  echo -e "No argument supplied, please specifiy a folder in your home directory."
  return
fi

echo -e "\nChecking if your USBs are plugged in..."
sleep 2s

if [ -d $USB1_PATH -a -d $USB2_PATH ]; then

  echo -e "> Hooray! Your USBs were found.\n"
  sleep 2s
  echo -e "Now checking if your cloud drive exists..."
  sleep 2s

  if [ -d $CLOUD_PATH ]; then

    echo -e "> Yes! Your cloud drive was found.\n"

    for folder in "$@"; do
      sleep 2s
      echo -e "Backing up '$folder' now...\n"

      folder_path="$(find . -type d -iname $folder -not -path "./$CLOUD_NAME/*")"
      usb1_folder_path=$(find $USB1_PATH -type d -iname $folder)
      usb2_folder_path=$(find $USB2_PATH -type d -iname $folder)
      cloud_folder_path=$(find $CLOUD_PATH -type d -iname $folder)

      echo -e "> Folder path: '$folder_path'\n"
#      echo $usb1_folder_path
#      echo $usb2_folder_path
#      echo $cloud_folder_path

      # checks if folder_path is not an empty string
      # as well as checking if the folder_path exists
      if [ ! -z "$folder_path" -a -d "$folder_path" ]; then

        if [ -d "$usb1_folder_path" -a -d "$usb2_folder_path" ]; then

          if [ -d "$cloud_folder_path" ]; then

            usb1_parent_path=$(echo $usb1_folder_path | sed s/"$folder"// )
            usb2_parent_path=$(echo $usb2_folder_path | sed s/"$folder"// )
            cloud_parent_path=$(echo $cloud_folder_path | sed s/"$folder"// )

#            echo $usb1_parent_path
#            echo $usb2_parent_path
#            echo $cloud_parent_path

            rm -rf $usb1_folder_path
            cp -r $folder_path $usb1_parent_path
            echo -e ""
            rm -rf $usb2_folder_path
            cp -r $folder_path $usb2_parent_path
            echo -e ""
            rm -rf $cloud_folder_path
            rsync -av --progress $folder_path $cloud_parent_path --exclude .git
            echo -e ""

          else
            echo -e "**'$folder' does not exist in your cloud drive.**"
            sleep 2s
            echo -e "> '$folder' not copied..."
            sleep 2s
            echo -e "> Please make sure the '$folder' directory exists in your cloud drive, then rerun this script.\n"
            sleep 2s
          fi

        else
          echo -e "**'$folder' does not exist in your USB drives.**"
          sleep 2s
          echo -e "> '$folder' not copied..."
          sleep 2s
          echo -e "> Please make sure the '$folder' directory exists in your USB drives, then rerun this script.\n"
        fi

      else
        echo -e "**'$folder' was not found in your home directory.**"
        sleep 2s
        echo -e "> '$folder' not copied..."
        sleep 2s
        echo -e "> Please make sure '$folder' exists in your home directory, then rerun this script.\n"
      fi

    done

    echo -e "\nThe backup script has finished!"
    sleep 2s
    echo -e "Exiting script now...\n"
    return

  else

  echo -e "Uh-oh! Your cloud drive could not be found."
  sleep 2s
  echo -e "Please activate your cloud drive directory and run this script again!"

  fi

else

  echo -e "Whoops! Your USBs aren''t plugged in."
  sleep 2s
  echo -e "Please plug in your USBs and run this script again!"

fi
