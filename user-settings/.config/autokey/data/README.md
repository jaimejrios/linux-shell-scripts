# Breezily Autokey

Breezily makes Ubuntu productivity a breeze.

How? By adding efficient keyboard commands so you can navigate your desktop with ease.

The [Autokey](https://github.com/autokey/autokey) program is used for implementing Breezily's keyboard commands.

[XEV](https://linux.die.net/man/1/xev) and [xmodmap](https://linux.die.net/man/1/xmodmap) is used for remapping the `Caps Lock` key to the `Hyper` key.

Also used are [xbindkeys](https://manpages.ubuntu.com/manpages/bionic/man1/xbindkeys.1.html) and [xdotool](https://manpages.ubuntu.com/manpages/impish/man1/xdotool.1.html) for generating mouse cursor movements with the keyboard (optional).

## Getting Autokey started

- We'll begin by installing the Autokey program and the Autokey script commands.
- Then we'll remap the `Caps Lock` key to the `Hyper` key using the `xmodmap` program.

1. Install `Autokey` via terminal with `sudo apt install autokey-gtk` or `sudo apt install autokey-qt`
2. Next, launch the `Autokey` program (it should appear in your applications list). This will create the `~/.config/Autokey/data` folder.
3. Open terminal and clone the `breezily-autokey/` repo into your `~/Downloads` folder.
4. With the terminal still open, run `cp ~/Downloads/breezily-autokey/autokey-hyper-scripts ~/.config/Autokey/data/` to copy the script commands to Autokey's data folder.
5. Next, enter `xev` in the terminal. Then, inside the `X Event Tester` window, press the `Caps Lock` key to view the specific keycode for it (remember this keycode).
6. Open the `~/Downloads/breezily-autokey/.Xmodmap` file in your text editor and change the `66` keycode value with the keycode obtained from running the `xev` program, then close the `Xmodmap` file.
    - If your `xev` keycode value is `66`, leave the `Xmodmap` file as is.
7. In your terminal, run `cp ~/Downloads/breezily-autokey/.Xmodmap ~/` to copy the `Xmodmap` file to your `~/` home folder.
8. Next, run `xmodmap .Xmodmap` in the terminal to activate the `Caps Lock` to `Hyper` key remapping.
9. [Edit your system startup setttings](https://linuxhint.com/run_linux_command_script_sys_reboot/) so that the `xmodmap .Xmodmap` command runs on login.
10. Launch the Autokey program. The `autokey-hyper-scripts` folder will appear under the `Name` menu. Also make sure to enable the setting `Automatically start Autokey at login` within Autokey's preferences panel.

## Installing the Mouse Pointer Movement Feature (optional)

- The Breezily `.xbindkeysrc` file uses the `xrandr` command to dynamically generate mouse cursor movement pixel values based on your screen's dimensions. These values are then stored in temporary variables.

- The `xdotool` program uses these variable values within the `xbindkeysrc` file to help move your mouse cursor (when you enter a Breezily command to move the mouse cursor, an `xdotool` command is also executed).

- After following the instructions below to setup, you will be able to move your mouse cursor with your keyboard. Using `Alt+W` for instance moves your mouse cursor up, `Alt+S` moves your cursor down, `Alt+A` moves the cursor left, etc. In addition, keyboard shortcuts will be added so you can snap windows into four screen quadrants (i.e. upper-left quadrant, upper-right quadrant, lower-left quadrant, etc.)

1. Open terminal and run `sudo apt install xbindkeys xdotool`
2. Copy the `.xbindkeysrc` into your `~/` (home) directory.
3. If `xbindkeys` does not startup automatically on login, edit your [system startup setttings](https://linuxhint.com/run_linux_command_script_sys_reboot/) and add the `xbindkeys` command to the list of startup programs.
4. Log out and log back in for the changes to take effect.

# Breezily Keyboard Shortcuts

Please note that the shortcut behvaior could vary depending on the app in use.

## Essential Shortcuts

- **Caps Lock-Z**: Undo the previous command. You can then press **Caps Lock-X** to Redo, reversing the undo command. In some apps, you can undo and redo multiple commands.
- **Caps Lock-C**: Copy the selected item to the Clipboard. This also works for files in the File Explorer.
- **Caps Lock-I**: Cut the selected item and copy it to the Clipboard.
- **Caps Lock-V**: Paste the contents of the Clipboard into the current document/app. This also works for files in the File Explorer.
- **Caps Lock-G**: Find items in a document or open the Find tool.
- **Caps Lock-S**: Save the current document/item.
- **Shift-Space**: Rename a selected file/item.
- **Caps Lock-Space**: Press the `Super` key.
- **Caps Lock-Q**: Press the `Escape` key.
- **Caps Lock-Shift-C**: Press the `Control-Shift-C` keys together.
- **Caps Lock-Shift-F**: Press the `Control-Shift-F` keys together.
- **Caps Lock-Shift-N**: Press the `Control-Shift-N` keys together.
- **Caps Lock-Shift-O**: Press the `Control-Shift-O` keys together.
- **Caps Lock-Shift-S**: Press the `Control-Shift-S` keys together.
- **Caps Lock-Shift-T**: Press the `Control-Shift-T` keys together.
- **Caps Lock-Shift-Tab**: Press the `Control-Shift-Tab` keys together.
- **Caps Lock-Alt-Tab**: Press the `Control-Shift-Tab` keys together.
- **Caps Lock-Shift-V**: Press the `Control-Shift-V` keys together.
- **Caps Lock-Shift-W**: Press the `Control-Shift-W` keys together.
- **Caps Lock-9**: Insert a unicode bullet character.
- **Caps Lock-Enter**: Press the `Control-Enter` keys together.

## Document Shortcuts

- **Caps Lock-D**: Delete the character to the right of the insertion point.
- **Caps Lock-H**: Delete the character to the left of the insertion point.
- **Caps Lock-F**: Move one character forward.
- **Caps Lock-B**: Move one character backward.
- **Caps Lock-P**: Move up one line.
- **Caps Lock-N**: Move down one line.
- **Caps Lock-O**: Scroll up on the current page.
- **Caps Lock-L**: Scroll down on the current page.
- **Caps Lock-Y**: Highlight/select all items.
- **Caps Lock-A**: Move to the beginning of the line/paragraph.
- **Caps Lock-E**:  Move to the end of the line/paragraph.
- **Caps Lock-K**: Delete text between the insertion point and the end of line/paragraph.
- **Caps Lock-Semicolon (;)**: Extend text selection one character to the left.
- **Caps Lock-Apostrophe (')**: Extend text selection one character to the right.
- **Caps Lock-Left Curly Brace ({)**: Extend text selection to beginning of the current word, then to the beginning of the following word if pressed again.
- **Caps Lock-Right Curly Brace (})**: Extend text selection to end of the current word, then to the end of the following word if pressed again.
- **Caps Lock-Backspace**: Select the text between the insertion point and the beginning of the current line.
- **Caps Lock-Backslash (\\)**: Select the text between the insertion point and the end of the current line.
- **Caps Lock-U**: Extend text selection to the nearest character at the same horizontal location on the line above.
- **Caps Lock-J**: Extend text selection to the nearest character at the same horizontal location on the line below.

## Window Shortcuts

- **Caps Lock-4**: Open a new window.
- **Caps Lock-W**: Close the active window.

## Web Browser Shortcuts

- **Caps Lock-R**: Reload the current page.
- **Caps Lock-T**: Open a new tab, and move to it.
- **Caps Lock-Tab**: Move to the next open tab.
- **Caps Lock-Shift-Tab**: Move to the previous open tab
- **Caps Lock-Backtick (`)**: Reopen the last closed tab, and move to it.
- **Caps Lock-1**: Open the previous page from your browsing history in the current tab.
- **Caps Lock-2**: Open the next page from your browsing history in the current tab.
- **Caps Lock-3**: Move to the address bar.
- **Caps Lock-0**: Return contents on the page to its default size.
- **Caps Lock-Minus sign (-)**: Decrease the size of the current page.
- **Caps Lock-Plus sign (+)**: Increase the size of the current page.

## Mouse Cursor Shortcuts (Optional)

- **Alt-C**: Click the left mouse button.
- **Alt-V**: Click the right mouse button.
- **Alt-W**: Move the mouse cursor up.
- **Alt-A**: Move the mouse cursor left.
- **Alt-S**: Move the mouse cursor down.
- **Alt-D**: Move the mouse cursor right.
- **Alt-Shift-W**: Move the mouse cursor up at double the distance.
- **Alt-Shift-A**: Move the mouse cursor left at double the distance.
- **Alt-Shift-S**: Move the mouse cursor down at double the distance.
- **Alt-Shift-D**: Move the mouse cursor right at double the distance.
- **Alt-Control-W**: Move the mouse cursor up at half the distance.
- **Alt-Control-A**: Move the mouse cursor left at half the distance.
- **Alt-Control-S**: Move the mouse cursor left at half the distance.
- **Alt-Control-D**: Move the mouse cursor left at half the distance.
- **Alt-1**: Move the mouse cursor to the top left of the screen.
- **Alt-2**: Move the mouse cursor to the top center of the screen.
- **Alt-3**: Move the mouse cursor to the top right of the screen.
- **Alt-4**: Move the mouse cursor to the middle left of the screen.
- **Alt-5**: Move the mouse cursor to the center of the screen.
- **Alt-6**: Move the mouse cursor to the middle right of the screen.
- **Alt-7**: Move the mouse cursor to the bottom left of the screen.
- **Alt-8**: Move the mouse cursor to the bottom center of the screen.
- **Alt-9**: Move the mouse cursor to the bottom right of the screen.
- **Alt-0**: Move the mouse cursor to the center of the current open window.
- **Alt-Shift-1**: Snaps the current window to the upper-left screen quadrant.
- **Alt-Shift-2**: Snaps the current window to the lower-left screen quadrant.
- **Alt-Shift-3**: Snaps the current window to the upper-right screen quadrant.
- **Alt-Shift-4**: Snaps the current window to the lower-right screen quadrant.
- **Alt-Shift-Backslash**: Minimize the current window.
- **Alt-Shift-0**: Maximize the current window.
- **Alt-Shift-Left Curly Brace**: Snap the current window to the left.
- **Alt-Shift-Right Curly Brace**: Snap the current window to the right.